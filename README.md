# GNGGML

Using Machine Learning to Study Defect Environment and Interfacial Thermal Resistance.

## Getting started

Before started, you should better to decompress the two compressed files.

## Final mechine learning model

Detailed instructions on how to use the model for perdictions are explained in this GitLab repo.

To predict the thermal properties for this case, the trained model can be used directly with the python program below:

```python
import tensorflow as tf
import pandas as pd
import numpy as np
import os

IF1 = [[1,0,0,0,1,0,0,2,2,2,3,2,1,7,0,3,1,3,2,0,0,1,0,0,1,0,0,2,0,0,0,0,0,0,0,0,1,0,0,2,2,2,0,0,0,1,2,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,1,1,0,0,2,0,0,2,0,1,0,3,1,1,2,4,1,3,2,0,1,3,0,4,0,0,78,1]]
MAX = [28,20,16,20,23,20,19,18,19,17,18,16,18,15,15,18,17,13,16,16,15,15,16,12,15,15,14,12,13,15,13,12,13,11,10,10,12,10,13,11,9,12,11,9,15,11,13,11,16,12,13,14,21,21,14,26,19,16,16,32,24,28,34,19,20,24,23,19,17,27,18,23,24,29,25,27,18,21,30,24,21,28,33,38,41,32,53,26,52,84,322,1]
IF1 = np.array(IF1)
MAX = np.array(MAX)
nor_IF1 = IF1/MAX

model1 = tf.keras.models.load_model('Trained')
print(model1.predict(nor_IF1).flatten())
```
